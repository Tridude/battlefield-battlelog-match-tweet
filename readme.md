Battlefield Battlelog Match Tweet
==============


A Python program that will post on Twitter about recent matches for a specified player.

Required modules:

python-twitter https://github.com/bear/python-twitter
Pyyaml http://pyyaml.org/
BeautifulSoup 4 http://www.crummy.com/software/BeautifulSoup/
