# okay
# BF4 version. Revert to old version for BF3
# Will adapt to both BF3 and 4 later
# Comment out post prompt section and untab for automatic posting.

import sys
import os
import argparse
import logging, logging.handlers
import urllib2
import re
import json

import twitter
from bs4 import BeautifulSoup
from yaml import load as yaml_load, dump as yaml_dump
try:
   from yaml import CLoader as YamlLoader, CDumper as YamlDumper
except ImportError:
   # default to python libyaml
   from yaml import Loader as YamlLoader, Dumper as YamlDumper

logging_levels = {
   0 : logging.NOTSET,
   1 : logging.DEBUG,
   2 : logging.INFO,
   3 : logging.WARNING,
   4 : logging.ERROR,
   5 : logging.CRITICAL,
}

battlelog_twitter_version = 1.0

# make sure script is first argument
script_dir = os.path.dirname(os.path.realpath(sys.argv[0]))

arg_parser = argparse.ArgumentParser(
   description="Script for tweeting latest played games on battlelog.")
arg_parser.add_argument("-v", "--version", action="store_true",
   help="Prints out script version.")
arg_parser.add_argument("-f", "--config-file", metavar="CONFIG_FILE", type=str,
   default="%s/battlelog_config.yaml"%script_dir,
   help="Path to configuration yaml file. Use full path if not running from script dir. Defaults to script_dir/battlelog_config.yaml")
arg_parser.add_argument("-L", "--log-level", metavar="LOG_LEVEL", type=int,
   choices=[0, 1, 2, 3, 4, 5], default=0,
   help="Corresponds to the logging levels of Python logging module. 0-5.")

args = arg_parser.parse_args()

if args.version:
   print "Battlelog twitter version: %s"%battlelog_twitter_version
   sys.exit(0)

logging_level = logging_levels.get(args.log_level, 0)

map_modes = {
   64 : "Conquest Large",
   128 : "Conqust Assault Large",
   2 : "Rush",
   8 : "Squad DM",
   2049 : "Team DM 16 Players",
   1024 : "Conquest Domination",
   4194304 : "Scavenger",
   8388608 : "Air Superiority",
   1 : "Conquest",
   256 : "Conqust Assault",
   4 : "Squad Rush",
   32 : "Team DM",
   512 : "Gun Master",
   131072 : "Tank Surperiority",
   524288 : "Capture The Flag",
   2097152 : "Obliteration",
}

map_names = {
   # Battlefield 3
   "MP_001" : "Grand Bazaar",
   "MP_003" : "Teheran Highway",
   "MP_007" : "Caspian Border",
   "MP_011" : "Seine Crossing",
   "MP_012" : "Operation Firestorm",
   "MP_013" : "Damavand Peak",
   "MP_017" : "Noshashar Canals",
   "MP_018" : "Kharg Island",
   "MP_Subway" : "Operation Metro",
   "XP1_001" : "Strike at Karkand",
   "XP1_002" : "Gulf of Oman",
   "XP1_003" : "Sharqi Peninsula",
   "XP1_004" : "Wake Island",
   "XP2_Factory" : "Scrapmetal",
   "XP2_Office" : "Operation 925",
   "XP2_Palace" : "Donya Fortress",
   "XP2_Skybar" : "Ziba Tower",
   "XP3_Desert" : "Bandar Desert",
   "XP3_Alborz" : "Alborz Mountains",
   "XP3_Shield" : "Armored Shield",
   "XP3_Valley" : "Death Valley",
   "XP4_Quake" : "Epicenter",
   "XP4_FD" : "Markaz Monolith",
   "XP4_Parl" : "Azadi Palace",
   "XP4_Rubble" : "Talah Market",
   "XP5_001" : "Operation Riverside",
   "XP5_002" : "Nebandan Flats",
   "XP5_003" : "Kiasar Railroad",
   "XP5_004" : "Sabalan Pipeline",

   # Battlefield 4
   "MP_Damage" : "Lancang Dam",
   "MP_Siege" : "Siege of Shanghai",
   "MP_Flooded" : "Flood Zone",
   "MP_Prison" : "Operation Locker",
   "MP_Journey" : "Golmud Railway",
   "MP_Naval" : "Paracel Storm",
   "MP_Tremors" : "Dawnbreaker",
   "MP_Abandoned" : "Zavod 311",
   "MP_Resort" : "Hainan Resort",
   "MP_TheDish" : "Rogue Transmission",
}

battlelog_url = "http://battlelog.battlefield.com/bf4"
version_battle = 4

# log_name = "battlelog.log"
# logger = logging.getLogger("battlelog")
# logger.setLevel()

try:
   f1 = open(args.config_file, 'r')
   config = yaml_load(f1, Loader=YamlLoader)
   f1.close()
except:
   print "cannot open config file. Exiting"
   sys.exit(2)

last_match_id = int(config['lastMatchID'])
print "last match id: %s"%last_match_id
userName = config['userName']
twitter_api = twitter.Api(
   consumer_key = config['tConsumerKey'],
   consumer_secret = config['tConsumerSecret'],
   access_token_key = config['tAccessTokenKey'],
   access_token_secret = config['tAccessTokenSecret']
)
try:
   twitter_api.VerifyCredentials()
except:
   print "Cannot connect to Twitter. Exiting"
   sys.exit(3)

try:
   profileSoup = BeautifulSoup(urllib2.urlopen('%s/user/%s/'%(battlelog_url, userName)).read())
except:
   print "Cannot open battlelog profile page."
   sys.exit(3)

if version_battle == 4:
   recent_games_class = "winlossgraph"
else:
   recent_games_class = "profile-gamereport-bar"

games = profileSoup.find('div', {'class':recent_games_class})
#print type(games)
#print games.find('a',href=True)
for match in reversed(games.find_all('a',href=True)):
   #print match['href']
   m = re.search('(\/\w*\/\w*\/*w\/\d*\/)(\d*)\/(\d*)\/', match['href'])
   match_id = int(m.group(2))
   player_id = m.group(3)
   if match_id <= last_match_id:
      print "not new match"
      continue
   print "match id:%s player id:%s"%(match_id, player_id)
   report_general_json = json.load(urllib2.urlopen('%s/battlereport/loadgeneralreport/%s/1/'%(battlelog_url, match_id)))
   report_player_json = json.load(urllib2.urlopen('%s/battlereport/loadplayerreport/%s/1/%s/'%(battlelog_url, match_id, player_id)))
   
   if version_battle == 4:
      try:
         player_json = report_general_json['players'][player_id]
      except:
         print "can't find player"
         continue
      kills = player_json['kills']
      deaths = player_json['deaths']
      score = player_json['combatScore']
      try:
         team_json = report_general_json['teams'][str(player_json['team'])]
      except:
         print "can't find team"
         continue
      team_name = team_json['name']
      if all(not report_team['isWinner'] for report_team in report_general_json['teams'].values()):
         result = "Draw"
      elif team_json['isWinner']:
         result = "Victory"
      else:
         result = "Defeat"

      try:
         match_map_mode = map_modes.get(report_general_json['gameServer']['mapMode'], report_general_json['gameServer']['mapMode'])
      except:
         print "can't find map mode"
         continue
      try:
         match_map_name = map_names.get(report_general_json['gameServer']['map'], report_general_json['gameServer']['map'])
      except:
         print "can't find map name"
         continue
      try:
         best_weapon_slug = report_player_json['best']['weapon']['slug']
      except:
         print "can't find weapon"
         best_weapon_slug = "None"
      try:
         best_class_slug = report_player_json['best']['class']['slug']
      except:
         print "can't find class"
         best_class_slug = "None"

      twitterMessage = 'Battlefield4: %s at %s -%s- %sp, %sk/%sd, weapon: %s, class: %s' % (match_map_mode, match_map_name, result, score, kills, deaths, best_weapon_slug, best_class_slug)
      print twitterMessage, len(twitterMessage)
      #a = raw_input("post?")
      #if a == 'y':
      status = twitter_api.PostUpdate("%s %s/battlereport/show/1/%s/%s/ #bf4"%(twitterMessage, battlelog_url, match_id, player_id))
      continue
   else:#if version_battle == 3:
      if report_general_json['gameReport'] == None:
         continue
      skip = False
      for faction in report_general_json['gameReport']['players']:
         found = False
         for player in report_general_json['gameReport']['players'][faction]:
            username = player['user']['username']
            if username != userName:
               continue
            found = True
            score = player['combatScore']
            if score == 0:
               print "no score"
               skip = True
               break
            match_map_mode = map_modes.get(report_general_json['gameReport']['gameServer']['mapMode'], "Unknown")
            match_map_name = map_names.get(report_general_json['gameReport']['gameServer']['map'], "Unknown")
            winner = report_general_json['gameReport']['winner']
            team = player['team']
            if team == winner:
               result = 'Victory'
            else:
               result = 'Defeat'
            kills = player['kills']
            deaths = player['deaths']
            break
         if found or skip:
            break
      if skip:
         continue

      report_player_json = json.load(urllib2.urlopen('%s/battlereport/loadplayerreport/%s/1/%s/'%(battlelog_url, match_id, player_id)))
      if report_player_json['topWeapon'] != None:
         best_weapon_slug = report_player_json['topWeapon']['slug']
      else:
         best_weapon_slug = None

      twitterMessage = 'Battlefield3: %s at %s -%s- %sp, %sk/%sd, weapon: %s #bf3' % (match_map_mode, match_map_name, result, score, kills, deaths, best_weapon_slug)
      print twitterMessage, len(twitterMessage)
      if len(twitterMessage) <= 140:
         pass
         #status = twitter_api.PostUpdate(twitterMessage)
         #print status
      else:
         print "Message over 140 characters"
         #status = twitter_api.PostUpdate(twitterMessage[:140])
         #print status



config['lastMatchID'] = int(match_id)
try:
   f1 = open(args.config_file, 'w')
   yaml_dump(config, f1, Dumper=YamlDumper, default_flow_style=False)
   f1.close()
except:
   print "Can't save yaml."
